# Spritely

A Docker environment to explore some generative art stuff in Python.

Building from Eric Davidson's post on [Dev.to](https://dev.to/thebuffed/how-to-create-generative-art-in-less-than-100-lines-of-code-1k9m)

## Docker

`docker-compose up --build` will create a sheet of sprites 9x9 in dimension with 32 sprites in an image 1500x1500 pixels.

Substitute the values of the run command to generate sprites with different characteristics.

`docker-compose run app python /app/spritely.py 7 24 1500`
